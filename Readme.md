Performing TILs detection using tile classfication.

Install conda environment from tils_env.yml
Run sbatch pr srun scripts in src directory to train models.

For new setup run, from the tils-detection-2019-2020 directory, assuming relative position for the Anaconda installation
```
cd ..
wget https://repo.anaconda.com/archive/Anaconda3-2019.10-Linux-x86_64.sh
sh Anaconda3-2019.10-Linux-x86_64.sh
rm Anaconda3-2019.10-Linux-x86_64.sh
```
Install the environment as:
```
conda install cudnn pytorch torchvision cudatoolkit=10.0.130 -c pytorch
conda env create -f tils_env.yml
```
Otherwise the steps to install everything from scratch are:
```
conda create -n tils_env python=3.7
conda activate tils_env
conda install cudnn pytorch torchvision cudatoolkit=10.0.130 -c pytorch
conda install -c conda-forge pyvips
pip install -r requirements.txt

```
For installing the python packages, pytorch-colors follow these steps
```
cd ../anaconda3/envs/tils_env/
git clone https://github.com/jorge-pessoa/pytorch-colors.git
cd pytorch-colors/
python setup.py install
cd ../../../../tils-detection-2019-2020/src
```

To download data run
```
cd ../data/
git config --global http.postBuffer 79999999999
git clone https://gitlab.com/Baggsy/tcga_slides_1.git
git clone https://gitlab.com/Baggsy/tcga_slides_2.git
git clone https://gitlab.com/Baggsy/tcga_slides_3.git
git clone https://gitlab.com/Baggsy/tcga_slides_4.git
mv tcga_slides_1/* .
mv tcga_slides_2/training/* training/
mv tcga_slides_3/training/* training/
mv tcga_slides_4/training/* training/
rm tcga_slides_* -rf
ls ./*/* -F |grep -v / | wc -l && du -s -h .
```

That should print:
```
76255
29G
```


