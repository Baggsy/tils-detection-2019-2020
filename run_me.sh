#!/bin/bash
# Run by executing command: `. run_me.sh`

env_name="tils_env"
echo "Activating conda environment: $env_name"
conda activate $env_name

dir_to_change="src"
cd $dir_to_change
count=`ls -1 *.sh 2>/dev/null | wc -l`
if [ $count != 0 ]
    then 
    ls *.sh
fi 

