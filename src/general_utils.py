import psutil as psutil
import time
import random
import numpy as np
import os
import datetime
import logging
import sys
import re
import json
import pickle

import torch
from torch.backends import cudnn

from dataset_utils import remove_norm

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


def get_time():
    return datetime.datetime.now().strftime('%d-%m_%H:%M')


def timeit(method):
    def timed(*args, **kw):
        t1 = time.time()
        result = method(*args, **kw)
        t2 = time.time()
        print('Method %s took %f sec to execute.' % (method.__name__, t2-t1))
        return result
    return timed


def init_experiment(config):
    exp_info = dict()

    # Adjust for debugging
    if config['debug']:
        exp_info['batch_size'] = 2
        exp_info['remove_dir_if_error'] = True
        exp_info['epochs'] = 2
        exp_info['num_augmentations'] = 2
        exp_info['num_workers'] = 1

    if config['model_weights_output'] == config['model_weights_overwrite_tag']:
        exp_info['model_weights_output'] = config['model_weights_to_load']

    if config['model_weights_to_load'] is not None:
        exp_info['pretraining'] = False

    # Random seeds and devices
    set_seed(config['seed'])
    if torch.cuda.is_available() and not config['cpu']:
        exp_info['cpu'] = False
        exp_info['device'] = torch.device('cuda')
        if config['verbosity'] >= 2:
            print(torch.__version__)
            print(torch.version.cuda)
            print(cudnn.version())
    else:
        exp_info['cpu'] = True
        exp_info['device'] = torch.device('cpu')

    # Experiment naming
    if config['resume_exp'] is None:
        exp_info['starting_epoch'] = 1
        exp_info['train_losses'] = []
        exp_info['eval_losses'] = []
        exp_info['train_metrics_list'] = []
        exp_info['eval_metrics_list'] = []
        exp_info['exp_dir'] = os.path.join(config['results_dir'], config['model'])
        if config['temp']:
            exp_name = "Exp_temp"
        else:
            if not os.path.isdir(exp_info['exp_dir']) or len(os.listdir(exp_info['exp_dir'])) == 0 \
                    or os.listdir(exp_info['exp_dir']) == ['Exp_temp']:
                exp_num = 1
            else:
                latest_exp_name = sorted([a for a in os.listdir(exp_info['exp_dir']) if not a == 'Exp_temp'],
                                         key=natural_keys)[-1]
                exp_num = int(latest_exp_name.split('_')[1]) + 1
            exp_name = "Exp_{}_{}_{}".format(exp_num, datetime.date.today(), "_".join(sys.argv))

        exp_info['exp_dir'] = os.path.join(exp_info['exp_dir'], exp_name)
        os.makedirs(exp_info['exp_dir'], exist_ok=True)
        exp_info['log'] = return_logger(os.path.join(exp_info['exp_dir'], config['log_filename']), config['debug'])
        exp_info['log'].warning('Initialised: {}'.format(exp_info['exp_dir']))
    else:
        exp_info['exp_dir'] = config['resume_exp']
        exp_info['model_weights_to_load'] = os.path.join(exp_info['exp_dir'], 'last_model_weights.pth')
        old_config = pickle.load(open(os.path.join(exp_info['exp_dir'], 'config.pickle'), 'rb'))
        exp_info['starting_epoch'] = old_config['last_epoch'] + 1
        exp_info['train_losses'] = old_config['train_losses']
        exp_info['eval_losses'] = old_config['eval_losses']
        exp_info['train_metrics_list'] = old_config['train_metrics_list']
        exp_info['eval_metrics_list'] = old_config['eval_metrics_list']
        exp_info['log'] = return_logger(os.path.join(exp_info['exp_dir'], old_config['log_filename']), config['debug'])
        exp_info['log'].warning('Resuming experiment: {} from {} epoch'.format(exp_info['exp_dir'], exp_info['starting_epoch']))

    # Early stopping
    if config['do_early_stopping']:
        exp_info['early_stopping'] = EarlyStopping(mode=config['early_stopping_mode'],
                                                   min_delta=config['early_stopping_delta'],
                                                   patience=config['early_stopping_patience'])

    config.update(exp_info)
    config['log'].info("Arguments: {}\n".format(sys.argv))
    config['log'].info(config)
    print_exp_info(config)


def initialise_epoch_log_info():
    epoch_info = dict()
    epoch_info['max_score'] = -1
    epoch_info['max_score_epoch'] = 0
    return epoch_info


def log_update_experiment_info(config, epoch_info, epoch, model):

    pid = os.getpid()
    py = psutil.Process(pid)
    mem_GB = "RAM: {:.3f}GB".format(py.memory_info()[0] / 1024/1024/1024)
    if not config['cpu']:
        mem_GB += ", GPU: {:.3f}GB".format(torch.cuda.max_memory_allocated(config['device'])/1024/1024/1024)
    log_str = '[{:2}/{:2}] | Loss {:.5f}, {:.5f} | ' \
              'Acc {:.4f}, {:.4f} | Recall {:.4f}, {:.4f} | MC {}, {} | {}'.format(epoch, config['epochs'],
                                                                               epoch_info['train_loss'][0],
                                                                               epoch_info['eval_loss'][0],
                                                                               epoch_info['train_metrics']['acc'],
                                                                               epoch_info['eval_metrics']['acc'],
                                                                               epoch_info['train_metrics']['recall'],
                                                                               epoch_info['eval_metrics']['recall'],
                                                                               epoch_info['train_miss_classifications'],
                                                                               epoch_info['eval_miss_classifications'],
                                                                               mem_GB)

    if not config['cpu']:
        torch.cuda.reset_max_memory_allocated(config['device'])
    config['log'].warning(log_str)
    config['train_losses'].append(epoch_info['train_loss'])
    config['eval_losses'].append(epoch_info['eval_loss'])
    config['train_metrics_list'].append(epoch_info['train_metrics'])
    config['eval_metrics_list'].append(epoch_info['eval_metrics'])
    config['last_epoch'] = epoch
    torch.save(model.state_dict(), os.path.join(config['exp_dir'], 'last_model_weights.pth'))
    pickle.dump({k: v for k, v in config.items() if not k == 'early_stopping'},
                open(os.path.join(config['exp_dir'], 'config.pickle'), 'wb'))

    if epoch_info['eval_metrics']['acc'] > epoch_info['max_score']:
        epoch_info['max_score'] = epoch_info['eval_metrics']['acc']
        epoch_info['max_eval_metrics'] = epoch_info['eval_metrics']
        epoch_info['max_score_epoch'] = epoch
        config['max_score_epoch_info'] = log_str
        torch.save(model.state_dict(), os.path.join(config['exp_dir'], 'best_model.pth'))

    if config['best_epoch_print']:
        config['log'].warning("-> Best eval: {}".format(epoch_info['max_eval_metrics']))


def save_plots_param(config, epoch_info):
    def _dump_json(list_of_floats):
        if isinstance(list_of_floats[0], dict):
            return [{k: json.dumps(float(v)) for k, v in epoch.items()} for epoch in list_of_floats]
        return [json.dumps(float(a)) for a in list_of_floats]

    def _retrieve_metrics(list_of_dicts):
        temp_dict = dict()
        for metric in list_of_dicts[0].keys():
            temp_dict[metric] = [v[metric] for v in list_of_dicts]
        return temp_dict

    config['log'].warning("\n--------\n Best score of {}, at epoch {} for exp ({}) \n   with info: {}\n--------".format(
        config['eval_metrics_list'][epoch_info['max_score_epoch']-1],
        epoch_info['max_score_epoch'],
        config['exp_dir'],
        config['max_score_epoch_info']))

    train_means = np.array([l[0] for l in config['train_losses']])
    train_stds = np.array([l[1] for l in config['train_losses']])
    eval_means = np.array([l[0] for l in config['eval_losses']])
    eval_stds = np.array([l[1] for l in config['eval_losses']])
    if config['plot_loss_std_area']:
        plt.plot(range(len(train_means)), train_means, c="blue")
        plt.fill_between(range(len(train_means)), train_means + train_stds, train_means - train_stds, facecolor='blue', alpha=0.1)
        plt.plot(range(len(eval_means)), eval_means, c="red")
        plt.fill_between(range(len(eval_means)), eval_means + eval_stds, eval_means - eval_stds, facecolor='orange',
                         alpha=0.1)
    else:
        plt.errorbar(range(len(train_means)), train_means, yerr=train_stds, c="blue", fmt='-o', ecolor="red", capsize=5)
        plt.errorbar(range(len(eval_means)), eval_means, yerr=eval_stds, c="orange", fmt='-o', ecolor="black", capsize=5)
    plt.xlabel('Epochs')
    plt.legend(['Train loss', 'Eval loss'])
    plt.grid()
    plt.savefig(os.path.join(config['exp_dir'], 'losses.png'))
    plt.close()

    metrics_train = _retrieve_metrics(config['train_metrics_list'])
    metrics_eval = _retrieve_metrics(config['eval_metrics_list'])
    for metric in metrics_train:
        if not (type(metrics_train[metric][0]) == np.ndarray or type(metrics_train[metric][0]) == str):
            plt.plot(metrics_train[metric])
            plt.plot(metrics_eval[metric])
            plt.xlabel('Epochs')
            plt.legend(['Train {}'.format(metric), 'Eval {}'.format(metric)])
            plt.grid()
            plt.savefig(os.path.join(config['exp_dir'], '{}.png'.format(metric)))
            plt.close()


def set_seed(seed):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed_all(seed)
        cudnn.enabled = True
        cudnn.benchmark = False
        cudnn.deterministic = True


def return_logger(log_file, log_debug):
    """
    Creates the logger
    """
    log = logging.getLogger(__name__)
    log.setLevel(logging.DEBUG if log_debug else logging.INFO)
    fh = logging.FileHandler(log_file)
    fh.setLevel(logging.DEBUG if log_debug else logging.INFO)
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter(fmt='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    log.addHandler(fh)
    log.addHandler(ch)

    return log


def print_exp_info(config):
    str_log =  \
        "\n================================\n"\
        "Running {} from epoch {}:\n"\
        "   Max num of epochs: {}\n"\
        "   Learning rate: {}\n" \
        "   batch_size: {}\n"\
        "   num_classes: {}\n" \
        "   weighted_loss: {}\n"\
        "   early_stopping: {}\n"\
        "   Device: {}\n"\
        "   Seed: {}\n"\
        "   Debugging mode: {}\n"\
        "================================".format(
            config['model'], config['starting_epoch'],
            config['epochs'],
            config['lr'],
            config['batch_size'],
            config['num_classes'],
            config['weighted_loss'],
            config['do_early_stopping'],
            config['device'] if config['cpu'] else torch.cuda.get_device_name(),
            config['seed'],
            config['debug']
           )

    config['log'].warning(str_log)


def labels_to_one_hot(labels, num_classes):
    labels = labels.cpu()
    cat_labels = np.array(labels).reshape(-1)
    return torch.from_numpy(np.eye(num_classes)[cat_labels]).float()


def natural_keys(text):
    """Returns the key list for float numbers in file names for more proper sorting"""
    def atof(text):
        """Used for sorting numbers, in float format"""
        try:
            retval = float(text)
        except ValueError:
            retval = text
        return retval
    return [atof(c) for c in re.split(r'[+-]?([0-9]+(?:[.][0-9]*)?|[.][0-9]+)', text)]


def plot_several_images(imgs, titles, config):
    title_map = {0: "Not TIL", 1: "TIL", -1: "-1"}
    cols = int(np.ceil(np.sqrt(len(imgs))))
    rows = int(np.ceil(len(imgs)/cols))
    f, axs = plt.subplots(cols, rows, figsize=(15, 15))
    for index, img in enumerate(imgs):
        if config['normalise_imgs']:
            img = remove_norm(img, config['channel_0_mean'], config['channel_1_mean'], config['channel_2_mean'],
                              config['channel_0_std'], config['channel_1_std'], config['channel_2_std']).astype(
                np.uint8)
        plt.subplot(cols, rows, index+1)
        plt.imshow(img)
        plt.axis('off')
        plt.title(title_map[titles[index]], fontsize=15)

    plt.tight_layout(w_pad=0.001)

    plt.savefig(os.path.join(config['exp_dir'], 'miss_classifications_{}.png'.format(
        0 if 'last_epoch' not in config else config['last_epoch'] + 1)))
    plt.close()


def plot_side_by_side(imgs, titles=None, show=True, size=None):
    def _get_numpy_arr(a):
        if not type(a) is np.ndarray:
            return np.array(a)
        return a
        
    num_imgs = len(imgs)
    n_r = int(np.sqrt(num_imgs))
    n_c = int(np.ceil(num_imgs/n_r))
    fig, axes = plt.subplots(nrows=n_r, ncols=n_c)
    fig.tight_layout()
    if size is not None:
        fig.set_size_inches((size, size))
    for indx, im in enumerate(imgs):
        im = _get_numpy_arr(im)   
        title_im = titles[indx] 
        plt.subplot(n_r, n_c, indx+1)
        plt.imshow(im)
        if title_im is not None:
            plt.title(title_im, fontsize=size)
            
    if show:
        plt.show()


class EarlyStopping(object):
    """
    The EarlyStopping object that keeps track on the previous values and determines convergence
    """
    def __init__(self, mode='min', min_delta=0, patience=10, percentage=False):
        self.mode = mode
        self.min_delta = min_delta
        self.patience = patience
        self.best = None
        self.num_bad_epochs = 0
        self.is_better = None
        self._init_is_better(mode, min_delta, percentage)

        if patience == 0:
            self.is_better = lambda a, b: True
            self.step = lambda a: False

    def step(self, metrics):
        if self.best is None:
            self.best = metrics
            return False

        if np.isnan(metrics):
            return True

        if self.is_better(metrics, self.best):
            self.num_bad_epochs = 0
            self.best = metrics
        else:
            self.num_bad_epochs += 1

        if self.num_bad_epochs >= self.patience:
            return True

        return False

    def _init_is_better(self, mode, min_delta, percentage):
        if mode not in {'min', 'max'}:
            raise ValueError('mode ' + mode + ' is unknown!')
        if not percentage:
            if mode == 'min':
                self.is_better = lambda a, best: a < best - min_delta
            if mode == 'max':
                self.is_better = lambda a, best: a > best + min_delta
        else:
            if mode == 'min':
                self.is_better = lambda a, best: a < best - (
                            best * min_delta / 100)
            if mode == 'max':
                self.is_better = lambda a, best: a > best + (
                            best * min_delta / 100)
