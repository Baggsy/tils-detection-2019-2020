import torch
from torch import optim, nn
from torch.hub import load_state_dict_from_url
import torchvision.models
import pytorch_colors as colors

from custom_models import Ensemble1, Yollo, Ensemble2, Ensemble3, Ensemble4, Ensemble5, Ensemble6, Resnet18Custom, load_pretrained_dict_from_url


def initialise_model(config):

    def _get_simple():
        return Simple(in_channels=config['img_width'] * config['img_height'] * config['img_channels'],
                      out_channels=config['num_classes'])

    def _get_inceptionv3():
        model = torchvision.models.inception_v3(num_classes=config['num_classes'], aux_logits=False)
        model = load_pretrained_dict_from_url(model, 'inceptionv3', config['pretraining'])
        config['img_width'] = 299
        config['img_height'] = 299

        return model

    def _get_densenet201():
        model = torchvision.models.densenet201(num_classes=config['num_classes'])
        model = load_pretrained_dict_from_url(model, 'densenet201', config['pretraining'])
        return model

    def _get_vgg16_bn():
        model = torchvision.models.vgg16_bn(num_classes=config['num_classes'])
        model = load_pretrained_dict_from_url(model, 'vgg16_bn', config['pretraining'])
        return model

    def _get_resnet152():
        model = torchvision.models.resnet152(num_classes=config['num_classes'])
        model = load_pretrained_dict_from_url(model, 'resnet152', config['pretraining'])
        return model

    def _get_resnet101():
        model = torchvision.models.resnet101(num_classes=config['num_classes'])
        model = load_pretrained_dict_from_url(model, 'resnet101', config['pretraining'])
        return model

    def _get_resnet18():
        model = torchvision.models.resnet18(num_classes=config['num_classes'])
        model = load_pretrained_dict_from_url(model, 'resnet18', config['pretraining'])
        return model

    def _get_resnet18_custom():
        model = Resnet18Custom(out_channels=config['num_classes'], in_channels=3)
        model = load_pretrained_dict_from_url(model, 'resnet18', config['pretraining'])
        return model

    def _get_resnet50():
        model = torchvision.models.resnet50(num_classes=config['num_classes'])
        model = load_pretrained_dict_from_url(model, 'resnet50', config['pretraining'])
        return model

    def _get_resnext101():
        model = torchvision.models.resnext101_32x8d(num_classes=config['num_classes'])
        model = load_pretrained_dict_from_url(model, 'resnext101', config['pretraining'])
        return model

    def _get_simple_colour():
        model = SimpleColourSpaces(in_channels=config['img_width'] * config['img_height'] * config['img_channels'],
                                   out_channels=config['num_classes'], hidden_dims=config['hidden_dims'], num_colour_spaces=config['num_colour_spaces'])
        config['normalise_imgs'] = False
        return model

    def _get_ensemble_1():
        return Ensemble1(out_channels=config['num_classes'], hidden_dims=config['hidden_dims_ensemble'],
                         num_heads=config['num_heads_ensemble'], img_height=config['img_height'])

    def _get_ensemble_2():
        return Ensemble2(out_channels=config['num_classes'], hidden_dims=config['hidden_dims_ensemble'],
                         num_heads=config['num_heads_ensemble'], img_height=config['img_height'])

    def _get_ensemble_3():
        return Ensemble3(out_channels=config['num_classes'], hidden_dims=config['hidden_dims_ensemble'],
                         num_heads=config['num_heads_ensemble'], img_height=config['img_height'])

    def _get_ensemble_4():
        return Ensemble4(out_channels=config['num_classes'], hidden_dims=config['hidden_dims_ensemble'],
                         num_heads=config['num_heads_ensemble'], img_height=config['img_height'])

    def _get_ensemble_5():
        return Ensemble5(out_channels=config['num_classes'], hidden_dims=config['hidden_dims_ensemble'],
                         num_heads=config['num_heads_ensemble'], img_height=config['img_height'])

    def _get_ensemble_6():
        return Ensemble6(out_channels=config['num_classes'], hidden_dims=config['hidden_dims_ensemble'],
                         num_heads=config['num_heads_ensemble'], img_height=config['img_height'])

    def _get_yollo():
        return Yollo(out_channels=config['num_classes'], img_height=config['img_height'])

    def _get_dummy():
        return Dummy(out_channels=config['num_classes'])

    switcher = {
        'simple': _get_simple,
        'dummy': _get_dummy,
        'inception': _get_inceptionv3,
        'densenet': _get_densenet201,
        'resnet150': _get_resnet152,
        'resnet18': _get_resnet18,
        'resnet18custom': _get_resnet18_custom,
        'resnet50': _get_resnet50,
        'resnet100': _get_resnet101,
        'resnext': _get_resnext101,
        'vgg': _get_vgg16_bn,
        'simple_colour': _get_simple_colour,
        'ensemble1': _get_ensemble_1,
        'ensemble2': _get_ensemble_2,
        'ensemble3': _get_ensemble_3,
        'ensemble4': _get_ensemble_4,
        'ensemble5': _get_ensemble_5,
        'ensemble6': _get_ensemble_6,
        'yollo': _get_yollo,
    }
    if config['model'].lower() not in switcher.keys():
        print("Model '{}' not included in options: {}".format(config['model'], switcher.keys()))
        raise ValueError
    nn_model = switcher.get(config['model'].lower())()
    optimiser = optim.Adam(nn_model.parameters(), lr=config['lr'])
    scheduler = None
    if config['lr_warmup']:
        lambda_funct = lambda epoch: (config['lr_warmup_min'] + (
                    (config['lr'] - config['lr_warmup_min']) / config['lr_warmup_epoch']) * epoch) if epoch <= config[
            'lr_warmup_epoch'] else config['lr'] * (0.8 ** epoch) + (config['lr_warmup_min'] + config['lr']) / 2
        scheduler = torch.optim.lr_scheduler.LambdaLR(optimiser, lr_lambda=lambda_funct)

    if config['model_weights_to_load'] is not None:
        config['log'].info('Loading model weights from: {}'.format(config['test_model_file']))
        nn_model.load_state_dict(torch.load(config['model_weights_to_load']))
    return {'model': nn_model.to(config['device']), 'opt': optimiser, 'scheduler': scheduler}


class Simple(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(Simple, self).__init__()
        self.net = nn.Linear(in_channels, out_channels)

    def forward(self, x):
        x = x.view(x.shape[0], -1)
        return torch.sigmoid(self.net(x))


class SimpleColourSpaces(nn.Module):
    def __init__(self, in_channels, out_channels, hidden_dims, num_colour_spaces):
        super(SimpleColourSpaces, self).__init__()
        self.nets = nn.ModuleList([])
        self.functs = list()
        nets = list()
        nets.append({'net': nn.Linear(in_channels, hidden_dims), 'funct': None})
        nets.append({'net': nn.Linear(in_channels, hidden_dims), 'funct': colors.rgb_to_hsv})
        nets.append({'net': nn.Linear(in_channels, hidden_dims), 'funct': colors.rgb_to_lab})
        nets.append({'net': nn.Linear(in_channels, hidden_dims), 'funct': colors.rgb_to_yuv})
        nets.append({'net': nn.Linear(in_channels, hidden_dims), 'funct': colors.rgb_to_ycbcr})
        nets.append({'net': nn.Linear(in_channels, hidden_dims), 'funct': colors.rgb_to_xyz})
        nets.append({'net': nn.Linear(in_channels, hidden_dims), 'funct': colors.rgb_to_hed})

        for i in range(num_colour_spaces):
            self.nets.append(nets[i]['net'])
            self.functs.append(nets[i]['funct'])

        self.classification = nn.Linear(hidden_dims*len(self.nets), out_channels)

    @staticmethod
    def _run_colour_net(x, b, net, funct):
        return torch.relu(net(x.view(b,-1) if funct is None else funct(x).view(b, -1)))

    def forward(self, x):
        assert x.shape[1] == 3 and x.min() >= 0 and x.max() <= 1
        colour_tokes = self._run_colour_net(x, x.shape[0], self.nets[0], self.functs[0])
        for i, net in enumerate(self.nets[1:]):
            colour_tokes = torch.cat((colour_tokes, self._run_colour_net(x, x.shape[0], net, self.functs[i+1])), dim=1)
        return self.classification(colour_tokes)


class Dummy(nn.Module):
    def __init__(self, out_channels, **kwargs):
        super(Dummy, self).__init__()
        self.out_channels = out_channels

    def forward(self, x):
        return torch.rand(x.shape[0], self.out_channels)
