import torch
from torch import optim, nn
import torch.nn.functional as F
from torch.hub import load_state_dict_from_url
import torchvision.models
import pytorch_colors as colors
import numpy as np
import random


def load_pretrained_dict_from_url(model, pth_name, pretraining):
    pretraining_pths = {
        'inceptionv3': 'https://download.pytorch.org/models/inception_v3_google-1a9a5a14.pth',
        'densenet201': 'https://download.pytorch.org/models/densenet201-c1103571.pth',
        'resnet152': 'https://download.pytorch.org/models/resnet152-b121ed2d.pth',
        'resnet101': 'https://download.pytorch.org/models/resnet101-5d3b4d8f.pth',
        'resnet50': 'https://download.pytorch.org/models/resnet50-19c8e357.pth',
        'resnet18': 'https://download.pytorch.org/models/resnet18-5c106cde.pth',
        'resnext101': 'https://download.pytorch.org/models/resnext101_32x8d-8ba56ff5.pth',
        'vgg16_bn': 'https://download.pytorch.org/models/vgg16_bn-6c64b313.pth'
    }

    layers_to_not_load = {
        'inceptionv3': ["fc.weight", "fc.bias", "AuxLogits.fc.weight", "AuxLogits.fc.bias"],
        'densenet201': ["classifier.weight", "classifier.bias"],
        'resnet152': ["fc.weight", "fc.bias"],
        'resnet101': ["fc.weight", "fc.bias"],
        'resnet50': ["fc.weight", "fc.bias"],
        'resnet18': ["fc.weight", "fc.bias"],
        'resnext101': ["fc.weight", "fc.bias"],
        'vgg16_bn': ["classifier.6.weight", "classifier.6.bias"]
    }

    if pretraining:
        url_path = pretraining_pths[pth_name]
        state_dict = load_state_dict_from_url(url_path)
        state_dict = {k: v for k, v in state_dict.items() if not (k in layers_to_not_load[pth_name])}
        print("Loading {} pre-trained weight matrices".format(len(state_dict.keys())))
        model.load_state_dict(state_dict, strict=False)
    return model

# TODO: colour space model. Predict choice of colour space (variable to be learnt), steerable conv kernels, circular structure. attention on selection.
#   batch norm, dropout, attention vs conv
#   residual connections like u-net
#   3 classes, the latest for niether TIL not not_TIL but also not correct info. -> corrupt image or load random biomedical images.
#   Uncertainty estimation: MC dropout, separate loss on uncertainty.
#   Randomise colour space transformation
#   Randomly dropout a few gradients from neurons. -> with reduced effect.
#   Fourier transformation
#   A memory residual connection: like flip-flop. Store the previous value
#   multi task. Classification + detection
#   After 20 epochs. Save weight values of most activated or higher valued neurons. Set them to zero for 5 epochs and learn all the rest. Then reset their values back. This will condense information and avoid over-fitting.
#   Convolution after deconvolution layers, Small encoder and decoder structures in place of simple convolution layers. Like small U-Net connections.
#   Warmup learning rate. Low at first, increase during the middle and decrease later.
#   Learn layers gradually. Like a baby


# self.functs = [None, colors.rgb_to_hsv, colors.rgb_to_lab, colors.rgb_to_yuv, colors.rgb_to_ycbcr,
#                        colors.rgb_to_xyz, colors.rgb_to_hed]
# self.funct_weights = [0.5] + [0.5/(len(self.functs)-1)]*(len(self.functs)-1)


class Ensemble1(nn.Module):
    def __init__(self, out_channels, hidden_dims, num_heads, img_height):
        super(Ensemble1, self).__init__()
        self.net1 = torchvision.models.resnet18(num_classes=hidden_dims)
        self.net2 = Yollo(out_channels=hidden_dims, img_height=img_height)
        self.classification = nn.Linear(hidden_dims*2, out_channels)

    def forward(self, x):
        # Expects input of [batch_size, height, width, 3]
        out_net1 = F.leaky_relu(self.net1(x))
        out_net2 = F.leaky_relu(self.net2(x))

        encoded_features = torch.cat((out_net1, out_net2), dim=-1)
        out = self.classification(encoded_features)
        return out


class Yollo(nn.Module):
    def __init__(self, out_channels, img_height, in_channels=3):
        super(Yollo, self).__init__()
        self.net = nn.Sequential(
            nn.Conv2d(in_channels=in_channels, out_channels=32, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(num_features=32, momentum=0.99, eps=0.001),
            nn.LeakyReLU(negative_slope=0.1),
            nn.MaxPool2d(kernel_size=2),

            nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(num_features=64, momentum=0.99, eps=0.001),
            nn.LeakyReLU(negative_slope=0.1),
            nn.MaxPool2d(kernel_size=2),

            nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(num_features=128, momentum=0.99, eps=0.001),
            nn.LeakyReLU(negative_slope=0.1),

            nn.Conv2d(in_channels=128, out_channels=256, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(num_features=256, momentum=0.99, eps=0.001),
            nn.LeakyReLU(negative_slope=0.1),
            nn.MaxPool2d(kernel_size=2),

            nn.Conv2d(in_channels=256, out_channels=512, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(num_features=512, momentum=0.99, eps=0.001),
            nn.LeakyReLU(negative_slope=0.1),

            nn.Conv2d(in_channels=512, out_channels=1024, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(num_features=1024, momentum=0.99, eps=0.001),
            nn.LeakyReLU(negative_slope=0.1),

            nn.Conv2d(in_channels=1024, out_channels=512, kernel_size=1, stride=1, padding=0, bias=False),
            nn.BatchNorm2d(num_features=512, momentum=0.99, eps=0.001),
            nn.LeakyReLU(negative_slope=0.1),
        )
        img_size_2 = int(np.floor(np.floor(np.floor(img_height/2)/2)/2))
        self.classification = nn.Linear(512*img_size_2*img_size_2, out_channels)

    def forward(self, x):
        x = self.net(x)
        x = self.classification(x.view(x.shape[0], -1))
        return x


class Ensemble2(nn.Module):
    def __init__(self, out_channels, hidden_dims, num_heads, img_height):
        super(Ensemble2, self).__init__()
        self.funct = colors.rgb_to_hsv

        self.net1 = torchvision.models.resnet18(num_classes=hidden_dims)
        self.net2 = Yollo(out_channels=hidden_dims, img_height=img_height)
        self.classification = nn.Linear(hidden_dims*2, out_channels)

    def forward(self, x):
        out_net1 = F.leaky_relu(self.net1(x))
        out_net2 = F.leaky_relu(self.net2(self.funct(x)))

        encoded_features = torch.cat((out_net1, out_net2), dim=-1)
        out = self.classification(encoded_features)
        return out


class Ensemble3(nn.Module):
    def __init__(self, out_channels, hidden_dims, num_heads, img_height):
        # Expects input of [batch_size, height, width, 3]
        super(Ensemble3, self).__init__()
        self.functs = [lambda a: a, colors.rgb_to_hsv, colors.rgb_to_lab, colors.rgb_to_yuv, colors.rgb_to_ycbcr,
                       colors.rgb_to_xyz, colors.rgb_to_hed]

        self.net1 = torchvision.models.resnet18(num_classes=hidden_dims)
        self.net2 = Yollo(out_channels=hidden_dims, img_height=img_height)

        self.classification = nn.Linear(hidden_dims * 2, out_channels)

    def forward(self, x):
        out_net1 = F.leaky_relu(self.net1(random.choice(self.functs)(x)))
        out_net2 = F.leaky_relu(self.net2(random.choice(self.functs)(x)))

        encoded_features = torch.cat((out_net1, out_net2), dim=-1)
        out = self.classification(encoded_features)
        return out


class Ensemble4(nn.Module):
    def __init__(self, out_channels, hidden_dims, num_heads, img_height):
        # Expects input of [batch_size, height, width, 3]
        super(Ensemble4, self).__init__()

        self.net1 = torchvision.models.resnet18(num_classes=hidden_dims)
        self.net2 = Yollo(out_channels=hidden_dims, img_height=img_height)

        self.q = nn.Linear(hidden_dims*2, hidden_dims)
        self.k = nn.Linear(hidden_dims*2, hidden_dims)
        self.v = nn.Linear(hidden_dims*2, hidden_dims)

        self.multihead_attention = nn.MultiheadAttention(embed_dim=1, num_heads=1)
        self.classification = nn.Linear(hidden_dims, out_channels)

    def forward(self, x):
        out_net1 = F.leaky_relu(self.net1(x))
        out_net2 = F.leaky_relu(self.net2(x))

        encoded_features = torch.cat((out_net1, out_net2), dim=-1)

        q = F.leaky_relu(self.q(encoded_features)).permute(1, 0).unsqueeze(2)
        k = F.leaky_relu(self.k(encoded_features)).permute(1, 0).unsqueeze(2)
        v = F.leaky_relu(self.v(encoded_features)).permute(1, 0).unsqueeze(2)

        attention_out = self.multihead_attention(q, k, v)[0].squeeze(2).permute(1, 0)
        out = self.classification(attention_out)
        return out


class Ensemble5(nn.Module):
    def __init__(self, out_channels, hidden_dims, num_heads, img_height):
        # Expects input of [batch_size, height, width, 3]
        super(Ensemble5, self).__init__()

        self.net1 = torchvision.models.resnet18(num_classes=hidden_dims)
        self.net2 = Yollo(out_channels=hidden_dims, img_height=img_height)

        self.q = nn.Linear(hidden_dims*2, hidden_dims)
        self.k = nn.Linear(hidden_dims*2, hidden_dims)
        self.v = nn.Linear(hidden_dims*2, hidden_dims)

        self.multihead_attention = nn.MultiheadAttention(embed_dim=hidden_dims, num_heads=num_heads)
        self.classification = nn.Linear(hidden_dims, out_channels)

    def forward(self, x):
        out_net1 = F.leaky_relu(self.net1(x))
        out_net2 = F.leaky_relu(self.net2(x))

        encoded_features = torch.cat((out_net1, out_net2), dim=-1)

        q = F.leaky_relu(self.q(encoded_features)).unsqueeze(0)
        k = F.leaky_relu(self.k(encoded_features)).unsqueeze(0)
        v = F.leaky_relu(self.v(encoded_features)).unsqueeze(0)

        attention_out = self.multihead_attention(q, k, v)[0].squeeze(0)
        out = self.classification(attention_out)
        return out


class Ensemble6(nn.Module):
    def __init__(self, out_channels, hidden_dims, num_heads, img_height):
        super(Ensemble6, self).__init__()
        self.net1 = load_pretrained_dict_from_url(torchvision.models.resnet18(num_classes=hidden_dims), 'resnet18', pretraining=True)
        self.net2 = Yollo(out_channels=hidden_dims, img_height=img_height)
        self.classification = nn.Linear(hidden_dims, out_channels)

    def forward(self, x):
        # Expects input of [batch_size, height, width, 3]
        out_net1 = F.leaky_relu(self.net1(x))
        out_net2 = F.leaky_relu(self.net2(x))

        encoded_features = torch.cat((out_net1.unsqueeze(-1), out_net2.unsqueeze(-1)), dim=-1).mean(dim=-1)
        out = self.classification(encoded_features)
        return out


class Resnet18Custom(nn.Module):
    def __init__(self, out_channels, in_channels=3):
        super(Resnet18Custom, self).__init__()

        self.relu = nn.LeakyReLU(inplace=True)
        self.net1 = nn.Sequential(
            nn.Conv2d(in_channels=in_channels, out_channels=64, kernel_size=7, stride=2, padding=3, bias=False),
            nn.BatchNorm2d(num_features=64),
            self.relu,
            nn.MaxPool2d(kernel_size=3, stride=2, padding=1, dilation=1, ceil_mode=False),
        )

        self.net2 = nn.Sequential(
            nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(num_features=64),
            self.relu,
            nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(num_features=64),
            self.relu,
            nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(num_features=64),
            self.relu,
            nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(num_features=64),
            self.relu,
        )

        self.net3_1 = nn.Sequential(
            nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(num_features=128),
            self.relu,
            nn.Conv2d(in_channels=128, out_channels=128, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(num_features=128),
        )

        self.net3_res = nn.Sequential(
            nn.Conv2d(in_channels=64, out_channels=128, kernel_size=1, stride=2, bias=False),
            nn.BatchNorm2d(num_features=128),
        )

        self.net3_2 = nn.Sequential(
            nn.Conv2d(in_channels=128, out_channels=128, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(num_features=128),
            self.relu,
            nn.Conv2d(in_channels=128, out_channels=128, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(num_features=128),
            self.relu,
        )

        self.net4_1 = nn.Sequential(
            nn.Conv2d(in_channels=128, out_channels=256, kernel_size=3, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(num_features=256),
            self.relu,
            nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(num_features=256),
        )

        self.net4_res = nn.Sequential(
            nn.Conv2d(in_channels=128, out_channels=256, kernel_size=1, stride=2, bias=False),
            nn.BatchNorm2d(num_features=256),
        )

        self.net4_2 = nn.Sequential(
            nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(num_features=256),
            self.relu,
            nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(num_features=256),
            self.relu,
        )

        self.net5_1 = nn.Sequential(
            nn.Conv2d(in_channels=256, out_channels=512, kernel_size=3, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(num_features=512),
            self.relu,
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(num_features=512),
        )

        self.net5_res = nn.Sequential(
            nn.Conv2d(in_channels=256, out_channels=512, kernel_size=1, stride=2, bias=False),
            nn.BatchNorm2d(num_features=512),
        )

        self.net5_2 = nn.Sequential(
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(num_features=512),
            self.relu,
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(num_features=512),
            self.relu,
        )
        self.avgpool = nn.AdaptiveAvgPool2d(output_size=(1, 1))
        self.classification = nn.Linear(512, out_channels)

        for modules in self.modules():
            if isinstance(modules, nn.Sequential):
                for m in modules:
                    if isinstance(m, nn.Conv2d):
                        nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                    elif isinstance(m, (nn.BatchNorm2d, nn.GroupNorm)):
                        nn.init.constant_(m.weight, 1)
                        nn.init.constant_(m.bias, 0)

        for m in self.modules():
            if isinstance(m, nn.Sequential):
                if isinstance(m[-2], nn.BatchNorm2d):
                    nn.init.constant_(m[-2].weight, 0)

    def forward(self, x):
        x = self.net1(x)
        x = self.net2(x)

        x = self.relu(self.net3_1(x) + self.net3_res(x))
        x = self.net3_2(x)

        x = self.relu(self.net4_1(x) + self.net4_res(x))
        x = self.net4_2(x)

        x = self.relu(self.net5_1(x) + self.net5_res(x))
        x = self.net5_2(x)

        x = self.avgpool(x)
        x = torch.flatten(x, 1)
        x = self.classification(x)
        return x