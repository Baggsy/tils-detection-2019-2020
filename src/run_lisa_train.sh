#!/bin/bash
#SBATCH -N 1
#SBATCH -t 00:10:00
#SBATCH --mem 32GB
#SBATCH -o ./log/train_%A_%a.out
#SBATCH -e ./log/train_%A_%a.err
#SBATCH --cpus-per-task=1

#SBATCH -p gpu_shared

mkdir -p log

# LISA cluster
module purge
module load 2019
module load CUDA/10.0.130

# E.g.: Run by executing either the srun command:
# `./run_lisa_train.sh param_configs/simple_colour_best`

HPARAMS_FILE="$1"
echo 'Parameter file: '$HPARAMS_FILE''

# Start the experiment.
echo "Param.: "$(head $HPARAMS_FILE)

python -u main.py $(head $HPARAMS_FILE)
if [[ $? = 0 ]]; then
    echo ""
    echo " ---- Finished running train.py ----"
    echo ""
else
    echo ""
    echo " ---- !X! Train.py failed !X! ---- "
    echo ""
fi

