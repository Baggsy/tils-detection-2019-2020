#!/bin/sh
#SBATCH -o ./log/train_%A_%a.out
#SBATCH -e ./log/train_%A_%a.err
#SBATCH --gres=gpu:1
#SBATCH --mem=30G
#SBATCH --time=10:00:00
#SBATCH --array=1-20%3

mkdir -p log/

# Specific to the Das4 cluster.
. /etc/bashrc
. /etc/profile.d/modules.sh
module load cuda10.0
module load cuda10.0/toolkit

# E.g.: Run by executing command `sbatch sbatch_das4_train.sh param_configs/simple_colour_best`

# echo run info
echo "SLURM_SUBMIT_DIR="$SLURM_SUBMIT_DIR
echo "SLURM_JOB_ID="$SLURM_JOB_ID
echo "SLURM_JOB_NAME="$SLURM_JOB_NAME

HPARAMS_FILE="$1"
echo 'Parameter file: '$HPARAMS_FILE''

# Start the experiment.
echo "Param.: "$(head -$SLURM_ARRAY_TASK_ID $HPARAMS_FILE | tail -1)

python -u main.py $(head -$SLURM_ARRAY_TASK_ID $HPARAMS_FILE | tail -1)
if [[ $? = 0 ]]; then
    echo ""
    echo " ---- Finished running train.py ----"
    echo ""
else
    echo ""
    echo " ---- !X! Train.py failed !X! ---- "
    echo ""
fi

