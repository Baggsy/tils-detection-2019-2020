import os
import numpy as np
from sklearn.metrics import f1_score, accuracy_score, recall_score, balanced_accuracy_score, confusion_matrix
import time

import torch
import torch.nn as nn

from general_utils import log_update_experiment_info, save_plots_param, initialise_epoch_log_info, return_logger, init_experiment, plot_several_images
from dataset_utils import initialise_data
from models import initialise_model

import matplotlib.pyplot as plt

# TODO: learn layers gradually
# TODO: dropout (gradually) learned neurons
# TODO: set to zero randomly learned neurons
# TODO: visualise conv layuers
# TODO: fully attention made net

# if not config.drop_plus_prob is None:
#             totally_the_same = True
#             for param_tensor in state2:
#                 mask_weights = torch.FloatTensor(state2[param_tensor].size()).uniform_() > config.drop_plus_prob
#                 model.state_dict()[param_tensor] = model.state_dict()[param_tensor].type(torch.FloatTensor).to(
#                     device) * mask_weights.type(torch.FloatTensor).to(device) + (
#                                                            1 - mask_weights.type(torch.FloatTensor).to(
#                                                        device)) * state2[param_tensor].type(
#                     torch.FloatTensor).to(device)
#                 if not (state2[param_tensor].cpu().numpy() == model.state_dict()[
#                     param_tensor].cpu().numpy()).all():
#                     totally_the_same = False
#             if totally_the_same:
#                 assert Exception('Totally the same')

def train(config):
    # Initialise experiment and model
    init_experiment(config)
    model_dict = initialise_model(config)

    train_dataloader, train_class_weights = initialise_data(config, mode='train')
    eval_dataloader, _ = initialise_data(config, mode='eval')

    model_dict['loss'] = nn.CrossEntropyLoss(
        weight=train_class_weights.to(config['device']) if config['weighted_loss'] else None).to(config['device'])

    # Iterate over epochs
    epoch_info = initialise_epoch_log_info()
    config['log'].info('[epoch/#epochs] | Loss Train, Eval | Acc Train, Eval | Recall Train, Eval | Miss Classifications Train, Eval | Mem')
    for epoch in range(config['starting_epoch'], config['epochs'] + 1):

        if config['learn_layers_gradually']:
            redefine_model(model_dict['model'])
            exit()

        model_dict['model'].train()
        epoch_info['train_loss'], epoch_info['train_metrics'], epoch_info['train_miss_classifications'] = perform_epoch(
            train_dataloader, model_dict, config,
            mode='train')

        model_dict['model'].eval()
        with torch.no_grad():
            epoch_info['eval_loss'], epoch_info['eval_metrics'], epoch_info['eval_miss_classifications'] = perform_epoch(
                eval_dataloader, model_dict, config,
                mode='eval')

        if config['lr_warmup']:
            model_dict['scheduler'].step()

        log_update_experiment_info(config, epoch_info, epoch, model_dict['model'])

        if config['do_early_stopping'] and config['early_stopping'].step(epoch_info['train_loss'][0]):
            config['log'].info("\n --- Early stopping criterion reached --- ")
            break

    if config['model_weights_output'] is not None:
        torch.save(model_dict['model'].state_dict(), config['model_weights_output'])
    save_plots_param(config, epoch_info)


def evaluate(config):
    if torch.cuda.is_available() and not config['cpu']:
        config['cpu'] = False
        config['device'] = torch.device('cuda')
    else:
        config['cpu'] = True
        config['device'] = torch.device('cpu')

    log_filename = config['test_log_filename']
    if 'Exp' in os.path.dirname(config['test_model_file']):
        log_filename = os.path.join(os.path.dirname(config['test_model_file']), log_filename)

    config['log'] = return_logger(log_filename, config['debug'])
    config['model_weights_to_load'] = config['test_model_file']
    config['pretraining'] = False
    config['normalise_imgs'] = False

    model_dict = initialise_model(config)
    test_dataloader, _ = initialise_data(config, mode='test')

    model_dict['model'].eval()
    img_predictions = dict()
    if not config['no_labels']:
        metrics = Metrics()
    with torch.no_grad():
        for batch_id, batch in enumerate(test_dataloader):
            data = batch['data'].to(config['device'])
            pred = model_dict['model'](data).detach().cpu().max(1)[1].numpy()
            img_paths = batch['img_path']
            for img_indx, img_path in enumerate(img_paths):
                img_predictions[img_path] = pred[img_indx]
            if not config['no_labels']:
                metrics.step(pred, batch['label'].cpu().numpy())

            if config['debug'] and batch_id > 0:
                break
    for k, v in img_predictions.items():
        config['log'].info("{} {}".format(k, v))
    if not config['no_labels']:
        config['log'].info('Metrics: {}'.format(metrics.get_metrics()))


def perform_epoch(dataloader, model_dict, config, mode):
    batch_losses = []
    metrics = Metrics()
    n_images = 0
    num_batches = len(dataloader)
    t0_batch = time.time()
    batch_miss_classifications_len = []

    means = []
    stds = []

    first_epoch = True if 'last_epoch' not in config.keys() else False

    if config['save_missclassifications'] and not mode == 'train':
        miss_classifications = []
        miss_classifications_titles = []

    for batch_id, batch in enumerate(dataloader):
        if mode == 'train':
            model_dict['opt'].zero_grad()

        t0 = time.time()
        data = batch['data'].to(config['device'])
        if config['print_batch_info'] or (first_epoch and batch_id == 0):
            print("Time for getting data: {:.5f} m".format((time.time()-t0)/60))
            print("Size of data: {:.4f} GB".format(data.element_size()*data.nelement()/1024/1024/1024))
            if not config['cpu']:
                try:
                    print("memory_summary: {}".format(torch.cuda.memory_summary()))
                except AttributeError:
                    pass
                print("memory_allocated: {}".format(torch.cuda.memory_allocated()))
                print("max_memory_allocated: {}".format(torch.cuda.max_memory_allocated()))
        labels = batch['label'].to(config['device'])

        data = data.view(-1, config['img_channels'], config['img_height'], config['img_width'])
        labels = labels.view(-1)

        n_images += data.shape[0]

        t1 = time.time()
        if config['model_run']:
            output = model_dict['model'](data)

        means.extend(torch.mean(data.view(data.shape[0], 3, -1), dim=-1).cpu().numpy())
        stds.extend(torch.std(data.view(data.shape[0], 3, -1), dim=-1).cpu().numpy())
        if config['print_batch_info'] or (first_epoch and batch_id == 0):
            print("{} data.shape: {}".format(mode, data.shape))
            print("{} data.range: ({}, {})".format(mode, data.min(), data.max()))

            print("Time to pass data through model: {:.5f} m".format((time.time() - t1) / 60))
            if not config['cpu']:
                print("memory_allocated: {}".format(torch.cuda.memory_allocated()))
                print("max_memory_allocated: {}".format(torch.cuda.max_memory_allocated()))

            if config['debug']:
                print("({}) Model output: {}".format(mode, output))
                print("Labels: {}".format(labels))

        if config['model_run']:
            loss = model_dict['loss'](output, labels)

            if mode == 'train':
                t2 = time.time()
                loss.backward()
                model_dict['opt'].step()
                if config['print_batch_info'] or (first_epoch and batch_id == 0):
                    print("Time to backropagate loss: {:.5f} m".format((time.time() - t2) / 60))
                    if not config['cpu']:
                        print("memory_allocated: {}".format(torch.cuda.memory_allocated()))
                        print("max_memory_allocated: {}".format(torch.cuda.max_memory_allocated()))

            batch_losses.append(loss.detach().cpu().numpy())
            preds = output.argmax(dim=1).detach().cpu().numpy()
            metrics.step(preds, labels.cpu().numpy())
            miss_imgs_indexes = np.where(preds != labels.cpu().numpy())[0]
            batch_miss_classifications_len.append(len(miss_imgs_indexes))

            if config['save_missclassifications'] and not mode == 'train':
                if len(miss_imgs_indexes) > 0 and len(miss_classifications) < config['num_missclassifications']:
                    if len(miss_imgs_indexes) > config['num_missclassifications'] - len(miss_classifications):
                        miss_imgs_indexes = np.random.choice(miss_imgs_indexes,
                                                             config['num_missclassifications'] - len(miss_classifications),
                                                             replace=False)
                    miss_classifications.extend(data.transpose(-3, -2).transpose(-2, -1).cpu().numpy()[miss_imgs_indexes])
                    miss_classifications_titles.extend(labels.cpu().numpy()[miss_imgs_indexes])

        if (config['print_batch_info'] or (first_epoch and batch_id == 0)) and batch_id % config['logging_batch_step'] == 0:
            print("[{:.3f}m] Batch iteration: {}/{}".format((time.time() - t0_batch) / 60, batch_id, num_batches))

        if config['debug'] and batch_id > 0:
            break

    if config['save_missclassifications'] and not mode == 'train' and len(miss_classifications) > 0:
        plot_several_images(miss_classifications, miss_classifications_titles, config)

    if config['print_batch_info'] or first_epoch or not config['model_run']:
        print("np.mean(sums); {}".format(np.mean(means, axis=0)))
        print("np.std(sums); {}".format(np.std(means, axis=0)))
        print("np.mean(stds); {}".format(np.mean(stds, axis=0)))
        print("np.std(stds); {}".format(np.std(stds, axis=0)))

    if not config['model_run']:
        print("{:.5f} minutes".format((time.time() - t0_batch) / 60))
        exit()

    if first_epoch:
        config['log'].info('n_{}_images: {}'.format(mode, n_images))

    return (np.mean(batch_losses), np.std(batch_losses)), metrics.get_metrics(), (
           np.around(np.mean(batch_miss_classifications_len), decimals=1), np.sum(batch_miss_classifications_len))


class Metrics:
    def __init__(self):
        self.metrics_pred = []
        self.metrics_true = []

    def step(self, pred, lbl):
        self.metrics_pred.extend(pred)
        self.metrics_true.extend(lbl)

    @staticmethod
    def _get_count_str(counts):
        counts_str = ""
        for l_indx, l in enumerate(counts[0]):
            counts_str += "{}: {}".format(l, counts[1][l_indx]) + (", " if l_indx < len(counts[0]) - 1 else "")
        return counts_str

    def get_metrics(self):
        true_counts = self._get_count_str(np.unique(self.metrics_true, return_counts=True))
        pred_counts = self._get_count_str(np.unique(self.metrics_pred, return_counts=True))
        return {'acc': balanced_accuracy_score(self.metrics_true, self.metrics_pred),
                'acc_unbalanced': accuracy_score(self.metrics_true, self.metrics_pred),
                'recall': recall_score(self.metrics_true, self.metrics_pred),
                'f1': f1_score(self.metrics_true, self.metrics_pred, average='binary'),
                "tn_fp_fn_tp": confusion_matrix(self.metrics_true, self.metrics_pred).ravel(),
                "true_counts": true_counts,
                "pred_counts": pred_counts
                }


def redefine_model(model):
    new_model = _create_new_model()
    w = _get_current_weights()
    model = new_model.load_state_dict(w)
    print("h")
