import os
import sys
import numpy as np
import pickle
import glob
import cv2
import pyvips
import matplotlib.pyplot as plt
import time
import multiprocessing
import sklearn
import random

import imgaug as ia
import imgaug.augmenters as iaa
from histomicstk.preprocessing.augmentation.color_augmentation import rgb_perturb_stain_concentration

import torch
from torch.utils.data import DataLoader, Dataset
import pytorch_colors as colors
import torchvision.transforms as transforms


class DatasetClass(Dataset):
    def __init__(self, config, args):

        if config['dataset'] == 'tcga':
            self.get_class_weights = self.get_class_weights_tcga
            self.load_data = load_data_tcga
            self.load_image_from_path = self.load_image_from_path_tcga
        else:
            print("Dataset name: '{}' is not recognised".format(config['dataset']))
            raise NotImplementedError

        self.mode = args['mode']
        if args['load_from_file'] is None:
            if config['verbosity'] >= 1:
                print("Creating data from directory '{}'".format(args['data_dir']))
            self.data = self.load_data(config, args)
        else:
            if config['verbosity'] >= 1:
                print("Loading pre-saved data from {}".format(args['load_from_file']))
            self.data = pickle.load(open(args['load_from_file'], "rb"))

        if args['save_to_file'] is not None:
            pickle.dump(self.data, open(args['save_to_file'], "wb"))

        self.width = config['img_width']
        self.height = config['img_height']
        self.channels = config['img_channels']
        self.permute_axis = config['permute_axis']
        self.no_labels = config['no_labels']
        self.pyvips = config['pyvips']
        self.max_scaling = 1/config['default_max_range_value'] if config['use_max_scaling'] else None

        self.normaliser = None
        if config['normalise_imgs']:
            self.normaliser = transforms.Compose([transforms.Normalize(
                mean=[config['channel_0_mean'], config['channel_1_mean'], config['channel_2_mean']],
                std=[config['channel_0_std'], config['channel_1_std'], config['channel_2_std']])])

        self.seq = None
        if config['augmented_training'] and args['mode'] == 'train' and config['num_augmentations'] > 0:
            self.num_augmentations = config['num_augmentations']
            self.seq = get_augment_object(config['effect_value'])

    def get_class_weights_tcga(self, batch_size):
        label_counts = {0: 0, 1: 0}
        for label in self.data['labels']:
            label_counts[label] += 1.0
        a = 1 / (batch_size * (label_counts[0] / self.__len__()))
        b = 1 / (batch_size * (label_counts[1] / self.__len__()))
        weights = torch.Tensor([a / (a + b), b / (a + b)])
        return weights

    def __len__(self):
        return len(self.data['data'])

    def __getitem__(self, index):
        img, aug_imgs = self.load_image_from_path(self.data['data'][index])

        label = torch.tensor([] if self.no_labels else self.data['labels'][index]).long()
        data = torch.tensor(img).float().unsqueeze(0)
        if aug_imgs is not None:
            data = torch.cat((data, torch.Tensor(aug_imgs).float()), dim=0)
            label = label.unsqueeze(0).repeat(self.num_augmentations + 1)

        if self.permute_axis:
            # To (channels, height, width) or (1 + num_augmentations, channels, height, width)
            data = data.transpose(-1, -2).transpose(-2, -3)

        if self.normaliser is not None:
            if len(data.shape) > 3:
                for img_indx, img in enumerate(data):
                    data[img_indx] = self.normaliser(img)
            else:
                data = self.normaliser(data)
        elif self.max_scaling is not None:
            data *= self.max_scaling

        batch_out = {'data': data, 'label': label}
        if self.mode == 'test':
            batch_out['img_path'] = self.data['data'][index]
        return batch_out

    def load_image_from_path_tcga(self, img_path):
        format_to_dtype = {
            'uchar': np.uint8,
            'char': np.int8,
            'ushort': np.uint16,
            'short': np.int16,
            'uint': np.uint32,
            'int': np.int32,
            'float': np.float32,
            'double': np.float64,
            'complex': np.complex64,
            'dpcomplex': np.complex128,
        }
        if self.pyvips:
            img = pyvips.Image.new_from_file(img_path, access='sequential')
            if not (self.height == img.height and self.width == img.width):
                y1 = max(0, int((img.height - self.height) / 2))
                x1 = max(0, int((img.width - self.width) / 2))
                img = img.crop(y1, x1, self.height, self.width)
            img = np.ndarray(buffer=img.write_to_memory(), dtype=format_to_dtype[img.format],
                             shape=[img.height, img.width, img.bands])[:, :, :self.channels]
        else:
            img = cv2.cvtColor(cv2.imread(img_path), cv2.COLOR_BGR2RGB)
            if not (self.height == img.shape[0] and self.width == img.shape[1]):
                x1 = max(0, int(img.shape[0] / 2 - self.height / 2))
                x2 = min(img.shape[0], int(img.shape[0] / 2 + self.height / 2))
                y1 = max(0, int(img.shape[1] / 2 - self.width / 2))
                y2 = min(img.shape[1], int(img.shape[1] / 2 + self.width / 2))
                img = img[x1:x2, y1:y2]
        assert img.shape == (self.height, self.width, self.channels)
        # Image: (height, width, channels) in int range of 0..255

        aug_imgs = None
        if self.seq is not None:
            try:
                aug_imgs = self.seq(images=np.stack([rgb_perturb_stain_concentration(img) for _ in range(self.num_augmentations)]))
            except (IndexError, ValueError, np.linalg.LinAlgError) as e:
                # png image does not contain other colours or range is not consisted for the histomicstk toolkit
                aug_imgs = self.seq(images=np.stack([img for _ in range(self.num_augmentations)], axis=0))

            # aug_imgs: (num_augmentations, height, width, channels)
        return img, aug_imgs


def load_data_tcga(config, args):
    data_list = []
    labels_list = []
    slides = np.random.permutation(os.listdir(args['data_dir']))
    if args['num_slides'] is not None:
        slides = slides[:args['num_slides']]

    for slide in slides:
        slide_path = os.path.join(args['data_dir'], slide)
        if not os.path.isdir(slide_path):
            continue
        config['log'].info("Loading {} image paths from {}".format(args['mode'], slide_path))
        if config['no_labels']:
            data_list.extend(glob.glob(os.path.join(slide_path, "*.png")))
        else:
            label_file_name = glob.glob(os.path.join(slide_path, "*.txt"))[0]
            with open(label_file_name, "r") as label_file:
                for line in label_file.readlines():
                    img_name, label = line.split()[:2]
                    label = int(label)
                    if label == -1:
                        label = 0
                    img_path = os.path.join(slide_path, img_name)
                    if not os.path.exists(img_path):
                        continue
                    data_list.append(img_path)
                    labels_list.append(label)
    if args['mode'] == 'train':
        data_list, labels_list = sklearn.utils.shuffle(data_list, labels_list)
    return {'data': data_list, 'labels': labels_list}


def get_augment_object(effect_value):
    # Check https://imgaug.readthedocs.io/en/latest/source/examples_basics.html
    seq = iaa.Sequential([
        iaa.Fliplr(0.5),
        iaa.Flipud(0.5),
        iaa.contrast.LinearContrast((0.85, 1.35)),
        iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05*255), per_channel=0.5),
        iaa.Multiply((0.8, 1.2), per_channel=0.2),
    ], random_order=True)

    return seq


def initialise_data(config, mode):
    allowed_modes = ['train', 'eval', 'test']
    if not mode in allowed_modes:
        assert Exception("mode '{}' not recognised. Allowed modes: {}".format(mode, allowed_modes))

    args = {'data_dir': config[mode + '_data_dir'], 'load_from_file': config[mode + '_data_load_from_file'],
            'save_to_file': config[mode + '_data_save_to_file'], 'mode': mode, 'num_slides': None}

    if config['small_dataset']:
        args['num_slides'] = config['small_dataset_num_{}_slides'.format(mode)]

    batch_size = config['batch_size'] if mode == "train" else config['batch_size']*(1 + config['num_augmentations'])

    dataset = DatasetClass(config, args)
    return DataLoader(dataset, batch_size=batch_size, num_workers=config['num_workers'], pin_memory=True,
                      shuffle=True if mode == 'train' else False), dataset.get_class_weights(
        batch_size=batch_size) if mode == "train" else None


def remove_norm(img, channel_0_mean, channel_1_mean, channel_2_mean, channel_0_std, channel_1_std, channel_2_std):
    stds = np.array([channel_0_std, channel_1_std, channel_2_std])
    means = np.array([channel_0_mean, channel_1_mean, channel_2_mean])
    return (img * stds) + means
