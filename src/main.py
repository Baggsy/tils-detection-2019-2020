import warnings
import os
import sys
import argparse
import time
import shutil
from train import train, evaluate

warnings.filterwarnings('ignore')


def main(config):
    try:
        if config['do_evaluation']:
            evaluate(config)
        else:
            train(config)

    except Exception as e:
        config['log'].error("\n\n ---------- Error: ---------- \n\n", exc_info=True)
        if config['remove_dir_if_error']:
            with open(os.path.join(config['exp_dir'], config['log_filename']), 'r') as log_file:
                with open("current_error_log.txt", 'w') as error_log:
                    error_log.write(log_file.read())
            shutil.rmtree(config['exp_dir'])
        raise e


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Cell detection')
    # Model specifics
    parser.add_argument("--seed", type=int, default=42)
    parser.add_argument("--model_weights_to_load", type=str, help='Name of weights file to load model')
    parser.add_argument("--model_weights_overwrite_tag", type=str, default="<same>",
                        help='Tag to check if overwrite of weights files is needed')
    parser.add_argument("--model_weights_output", type=str,
                        help='Name of file to save top model wights. If model_weights_overwrite_tag then '
                             'it overwrites the model_weights_to_load')
    parser.add_argument("--model", type=str, default="simple")
    parser.add_argument("--num_classes", type=int, default=2)
    parser.add_argument("--batch_size", type=int, default=32)
    parser.add_argument("--lr", type=float, default=1e-4)
    parser.add_argument("--epochs", type=int, default=100)
    parser.add_argument("--resume_exp", type=str, help="Directory of exp to resume from e.g.: '../results/Simple/exp/'")
    parser.add_argument("--log_filename", type=str, default='output.log')
    parser.add_argument("--results_dir", type=str, default='../results')
    parser.add_argument("--save_model_to_file", type=str, default=None,
                        help='which file to save nn model to after training')
    parser.add_argument("-no_weighted_loss", dest="weighted_loss", action="store_false")
    parser.add_argument("-no_pretraining", dest="pretraining", action="store_false")
    parser.add_argument("-lr_warmup", dest="lr_warmup", action="store_true")
    parser.add_argument("--lr_warmup_epoch", type=int, default=5)
    parser.add_argument("--lr_warmup_min", type=float, default=1e-3)
    parser.add_argument("-learn_layers_gradually", dest="learn_layers_gradually", action="store_true")

    # Colour space
    parser.add_argument("--hidden_dims", type=int, default=10)
    parser.add_argument("--num_colour_spaces", type=int, default=2)

    # Ensemble model
    parser.add_argument("--hidden_dims_ensemble", type=int, default=100)
    parser.add_argument("--num_heads_ensemble", type=int, default=4)

    # Data loading specific
    parser.add_argument("--dataset", type=str, default="tcga")
    parser.add_argument("--num_workers", type=int, default=4)
    parser.add_argument("--train_data_dir", type=str, default="../data/training/")
    parser.add_argument("--train_data_load_from_file", type=str)
    parser.add_argument("--train_data_save_to_file", type=str, help='e.g.: train_data')
    parser.add_argument("--eval_data_dir", type=str, default="../data/eval/")
    parser.add_argument("--eval_data_load_from_file", type=str)
    parser.add_argument("--eval_data_save_to_file", type=str, help='e.g.: eval_data')
    parser.add_argument("--img_width", type=int, default=100)
    parser.add_argument("--img_height", type=int, default=100)
    parser.add_argument("--img_channels", type=int, default=3)
    parser.add_argument("-no_axis_permutation", dest="permute_axis", action="store_false")
    parser.add_argument("-no_img_normalisation", dest="normalise_imgs", action="store_false")
    parser.add_argument("-pyvips", dest="pyvips", action="store_true")
    parser.add_argument("-small_dataset", dest="small_dataset", action="store_true")
    parser.add_argument("--small_dataset_num_train_slides", type=int, default=10)
    parser.add_argument("--small_dataset_num_eval_slides", type=int, default=3)
    parser.add_argument("--default_max_range_value", type=float, default=255)
    parser.add_argument("-no_max_scaling", dest="use_max_scaling", action="store_false")
    parser.add_argument("--channel_0_mean", type=float, default=181.25291)
    parser.add_argument("--channel_1_mean", type=float, default=133.75212)
    parser.add_argument("--channel_2_mean", type=float, default=171.22366)
    parser.add_argument("--channel_0_std", type=float, default=41.13408)
    parser.add_argument("--channel_1_std", type=float, default=44.874393)
    parser.add_argument("--channel_2_std", type=float, default=34.757454)

    # Testing
    parser.add_argument("-do_evaluation", action="store_true")
    parser.add_argument("--test_model_file", type=str,
                        help="model filename to use e.g.: '../results/simple_colour/Exp_8_2019-12-12/best_model.pth'")
    parser.add_argument("--test_data_dir", type=str, default="../data/eval/")
    parser.add_argument("--test_data_load_from_file", type=str)
    parser.add_argument("--test_data_save_to_file", type=str, help='e.g.: test_data')
    parser.add_argument("--test_log_filename", type=str, default='test_output.log')
    parser.add_argument("-no_labels", action="store_true")

    # Image augmentation
    parser.add_argument("--num_augmentations", type=int, default=10)
    parser.add_argument("--effect_value", type=float, default=0.5)
    parser.add_argument("-no_augmented_training", dest="augmented_training", action="store_false")

    # Early stopping
    parser.add_argument("-no_early_stopping", dest='do_early_stopping', action='store_false')
    parser.add_argument("--early_stopping_mode", type=str, default='min')
    parser.add_argument("--early_stopping_delta", type=float, default=0.005)
    parser.add_argument("--early_stopping_patience", type=int, default=5)

    # For debugging
    parser.add_argument("-d", dest="debug", action="store_true")
    parser.add_argument("-cpu", action="store_true")
    parser.add_argument("--verbosity", type=int, default=1)
    parser.add_argument("-t", dest="temp", action="store_true")
    parser.add_argument("-remove_dir_if_error", action="store_true")
    parser.add_argument("--logging_batch_step", type=int, default=30)
    parser.add_argument("-print_batch_info", action="store_true")
    parser.add_argument("-no_model_run", dest='model_run', action="store_false")
    parser.add_argument("-no_best_epoch_print", dest='best_epoch_print', action="store_false")
    parser.add_argument("-no_missclassification_saving", dest="save_missclassifications", action="store_false")
    parser.add_argument("--num_missclassifications", type=int, default=25)
    parser.add_argument("-no_std_area_plot", dest='plot_loss_std_area', action="store_false")
    args = parser.parse_args()

    t0 = time.time()
    main(vars(args))
    print("\n-------------- Done in {0:.4f} [hours]---------------".format((time.time() - t0) / (60 * 60)))
