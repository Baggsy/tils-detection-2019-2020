#!/bin/bash

# Das4 cluster.
. /etc/bashrc
. /etc/profile.d/modules.sh
module load cuda10.0
module load cuda10.0/toolkit

# E.g.: Run by executing command `./srun_das4_train.sh param_configs/simple_colour_best`

HPARAMS_FILE="$1"
echo 'Parameter file: '$HPARAMS_FILE''

# Start the experiment.
echo "Param.: "$(head $HPARAMS_FILE)

srun --time=05:00:00 --mem=20GB --gres=gpu:1 python -u main.py $(head $HPARAMS_FILE)
if [[ $? = 0 ]]; then
    echo ""
    echo " ---- Finished running train.py ----"
    echo ""
else
    echo ""
    echo " ---- !X! Train.py failed !X! ---- "
    echo ""
fi

